package com.company.util;

/**
 * @author Pavlo Khmarnyi
 * @since 11/2/2019
 */
public class ConnectionParams {
    public static final String URL = "jdbc:mysql://localhost:3306/users";
    public static final String USER = "root";
    public static final String PASSWORD = "root";
}
