package com.company;

import com.company.dao.BookDao;
import com.company.dao.UserDao;
import com.company.dao.impl.BookDaoImpl;
import com.company.dao.impl.UserDaoImpl;
import com.company.datasource.Application;
import com.company.model.Book;
import com.company.model.User;

import java.util.List;

/**
 * @author Pavlo Khmarnyi
 * @since 10/30/2019
 */
public class DatabaseApplication {

    public static void main(String[] args) {
        System.out.println("Application started!");
        System.out.println("getAllUsers()\n");
        UserDao userDao = new UserDaoImpl(Application.INSTANCE.dataSource());
        BookDao bookDao = new BookDaoImpl(Application.INSTANCE.dataSource());

        System.out.println("\n======UserDao======\n");

        List<User> users = userDao.getAllUsers();

        for (User user: users) {
            System.out.println("---------------");
            System.out.println("id = " + user.getId());
            System.out.println("First name: " + user.getFirstName());
            System.out.println("Last name: " + user.getLastName());
            System.out.println("Age: " + user.getAge());
            System.out.println("Email: " + user.getEmail());
            System.out.println("Gender: " + user.getGender());
        }

        System.out.println("\ngetUserById()\n");
        User user = userDao.getUserById(2L);
        System.out.println("User with id: "
                + user.getId()
                + "\nFirst name: "
                + user.getFirstName()
                + "\nLast name: "
                + user.getLastName());

        System.out.println("\nsaveUser()\n");
        User peter = new User();
        peter.setFirstName("Shon");
        peter.setLastName("Pen");
        peter.setAge(28);
        peter.setEmail("shon.pen@email.com");
        peter.setGender("male");
        Long id = userDao.saveUser(peter);
        System.out.println("Id = " + id);

        System.out.println("\nupdateUser()\n");
        peter.setId(id);
        peter.setBookId(2L);
        Long userId = userDao.updateUser(peter);
        System.out.println("Modified user with id = " + userId);

        System.out.println("\ndeleteUser()\n");
        Long deleteId = userDao.deleteUser(peter.getId());
        System.out.println("User with id = " + deleteId + " was deleted");

        System.out.println("\n======BookDao======\n");
        System.out.println();
        System.out.println("\ngetAllBooks()\n");
        List<Book> books = bookDao.getAllBooks();
        for (Book book: books) {
            System.out.println("---------------");
            System.out.println("id = " + book.getId());
            System.out.println("Title: " + book.getTitle());
            System.out.println("Author: " + book.getAuthor());
            System.out.println("Genre: " + book.getGenre());
        }

        System.out.println("\ngetBookById()\n");
        Book book = bookDao.getBookById(3L);
        System.out.println("Book title: " + book.getTitle());
        System.out.println();
        Book savedBook = new Book();
        savedBook.setTitle("Think in java");
        savedBook.setAuthor("Ekkel");
        savedBook.setGenre("Technic");
        Long savedId = bookDao.saveBook(book);
        System.out.println("savedId = " + savedId);

        System.out.println("\nupdateBook()\n");
        savedBook.setId(savedId);
        savedBook.setGenre("Programming");
        Long updatedId = bookDao.updateBook(savedBook);
        System.out.println("updatedId = " + updatedId);
        System.out.println("\ndeleteBook()\n");
        Long deletedId = bookDao.deleteBook(savedBook.getId());
        System.out.println("deletedId = " + deletedId);

        System.out.println("\nApplication finished!");
    }
}
