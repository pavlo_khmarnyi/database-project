package com.company.datasource;

import static com.company.util.ConnectionParams.*;

import javax.sql.DataSource;
import com.mysql.cj.jdbc.MysqlDataSource;

public enum Application {
    INSTANCE;

    private DataSource dataSource;

    public DataSource dataSource() {
        if (dataSource == null) {
            MysqlDataSource dataSource = new MysqlDataSource();
            dataSource.setUser(USER);
            dataSource.setPassword(PASSWORD);
            dataSource.setURL(URL);
            this.dataSource = dataSource;
        }
        return dataSource;
    }
}
