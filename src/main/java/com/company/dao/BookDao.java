package com.company.dao;

import com.company.model.Book;

import java.util.List;

/**
 * @author Pavlo Khmarnyi
 * @since 11/2/2019
 */
public interface BookDao {
    List<Book> getAllBooks();
    Book getBookById(Long id);
    Long saveBook(Book book);
    Long updateBook(Book book);
    Long deleteBook(Long id);
}
