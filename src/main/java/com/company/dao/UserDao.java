package com.company.dao;

import com.company.model.User;

import java.util.List;

/**
 * @author Pavlo Khmarnyi
 * @since 11/2/2019
 */
public interface UserDao {
    List<User> getAllUsers();
    User getUserById(Long id);
    Long saveUser(User user);
    Long updateUser(User user);
    Long deleteUser(Long id);
}
