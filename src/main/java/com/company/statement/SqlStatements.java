package com.company.statement;

/**
 * @author Pavlo Khmarnyi
 * @since 11/2/2019
 */
public class SqlStatements {
    public static final String SELECT_ALL_USERS = "SELECT * FROM user";
    public static final String SELECT_USER = "SELECT * FROM user WHERE id = ?";
    public static final String INSERT_USER = "INSERT INTO user (first_name, last_name, age, email, gender)" +
            " VALUES (? ,? ,? ,? ,?)";
    public static final String UPDATE_USER = "UPDATE user " +
            "SET first_name = ?, " +
            "last_name = ?, " +
            "age = ?, " +
            "email = ?, " +
            "gender = ?, " +
            "book_id = ? " +
            "WHERE id = ?";
    public static final String DELETE_USER = "DELETE FROM user WHERE id = ?";

    public static final String SELECT_ALL_BOOKS = "SELECT * FROM book";
    public static final String SELECT_BOOK = "SELECT * FROM book WHERE id = ?";
    public static final String INSERT_BOOK = "INSERT INTO book (title, author, genre)" +
            " VALUES (? ,? ,?)";
    public static final String UPDATE_BOOK = "UPDATE book " +
            "SET title = ?, " +
            "author = ?, " +
            "genre = ? " +
            "WHERE id = ?";
    public static final String DELETE_BOOK = "DELETE FROM book WHERE id = ?";
}
