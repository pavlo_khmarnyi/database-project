INSERT INTO book (title, author, genre)
VALUES ('Hamlet', 'Shakespeare', 'Drama');

INSERT INTO book (title, author, genre)
VALUES ('King Lear', 'Shakespeare', 'Drama');

INSERT INTO book (title, author, genre)
VALUES ('Puaro', 'Ahata', 'detective');

INSERT INTO book (title, author, genre)
VALUES ('Smog', 'King', 'scary');

INSERT INTO book (title, author, genre)
VALUES ('Birds', 'King', 'scary');

INSERT INTO user (first_name, last_name, age, email, gender)
VALUES ('John', 'Smith', 25, 'john.smith@email.com', 'male');

INSERT INTO user (first_name, last_name, age, email, gender)
VALUES ('Sara', 'Smith', 22, 'sara.smith@email.com', 'female');

INSERT INTO user (first_name, last_name, age, email, gender)
VALUES ('Max', 'Anderson', 27, 'max.anderson@email.com', 'male');

INSERT INTO user (first_name, last_name, age, email, gender)
VALUES ('Marry', 'Anderson', 24, 'marry.anderson@email.com', 'female');

UPDATE user SET book_id = 1 WHERE first_name = 'John' AND last_name = 'Smith';
UPDATE user SET book_id = 2 WHERE first_name = 'Sara' AND last_name = 'Smith';
UPDATE user SET book_id = 4 WHERE first_name = 'Max';
UPDATE user SET book_id=(SELECT id FROM book WHERE title='King') WHERE first_name='Marry';
