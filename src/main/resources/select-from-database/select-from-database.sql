SELECT * FROM user;
SELECT email FROM user;
SELECT * FROM user WHERE last_name='Smith';
SELECT email FROM user WHERE last_name='Smith';

SELECT email FROM user JOIN book on user.book_id = book.id;

SELECT email, author FROM user JOIN book on user.book_id = book.id;

SELECT email, author FROM user LEFT JOIN book on user.book_id = book.id;

SELECT email, author FROM user INNER JOIN book on user.book_id = book.id;

SELECT email, author FROM user RIGHT JOIN book on user.book_id = book.id;

SELECT email, title FROM user JOIN book ON user.book_id = book.id WHERE book.author='Shakespeare';
